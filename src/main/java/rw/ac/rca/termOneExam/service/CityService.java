package rw.ac.rca.termOneExam.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.repository.ICityRepository;
import rw.ac.rca.termOneExam.utils.APICustomResponse;

@Service
public class CityService {

	@Autowired
	private ICityRepository cityRepository;
	
	public ResponseEntity<APICustomResponse> getById(long id) {
	Optional<City> cityFoundById = cityRepository.findById(id);
	if(cityFoundById.isPresent()){
	City city = cityFoundById.get();
	city.setFahrenheit((city.getWeather()*9/5)+32);
		return ResponseEntity.ok(new APICustomResponse(true,"",city));
	}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new APICustomResponse(false,"City not found"));
	}

	public List<City> getAll() {
		
		return cityRepository.findAll();
	}

	public boolean existsByName(String name) {
		
		return cityRepository.existsByName(name);
	}
	public ResponseEntity<APICustomResponse> findCityByWeatherGreaterThan(Double weather){
		Optional<City> cityFound = cityRepository.findCityByWeatherGreaterThan(weather);
		if(cityFound.isPresent()){
			City city = cityFound.get();
			return ResponseEntity.ok(new APICustomResponse(true,"City found",city));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new APICustomResponse(false,"City with the weather less than "+weather+" not found"));
	}

	public ResponseEntity<APICustomResponse> findCityByWeatherLessThan(Double weather){
		Optional<City> cityFound = cityRepository.findCityByWeatherLessThan(weather);
		if(cityFound.isPresent()){
			City city = cityFound.get();
			return ResponseEntity.ok(new APICustomResponse(true,"City found",city));
		}
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new APICustomResponse(false,"City with the weather greater than "+weather+" not found"));
	}

	public ResponseEntity<APICustomResponse> save(CreateCityDTO dto) {
		City city =  new City(dto.getName(), dto.getWeather());
//		return cityRepository.save(city);


		if (cityRepository.existsByName(dto.getName())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(new APICustomResponse(false, "City name " + dto.getName() + " is registered already"));
		}

		City savedCity = cityRepository.save(city);
		return ResponseEntity.status(HttpStatus.CREATED).body(new APICustomResponse(true,"City created successfully", city));


//		Product product = new Product(productDto);
//		if(productRepository.existsByName(product.getName())){
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new APIResponse(false,"Product name already exists"));
//		}
//		Product savedProduct = productRepository.save(product);
//		return ResponseEntity.status(HttpStatus.CREATED).body(new APIResponse(true, "Product created successfully", savedProduct));

	}



}
