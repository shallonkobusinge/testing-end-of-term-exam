package rw.ac.rca.termOneExam.utils;

import rw.ac.rca.termOneExam.domain.City;

public class APICustomResponse {

	private boolean status;
	
	private String message;

	private City city;


	public APICustomResponse(boolean status, String message, City city) {
		this.status = status;
		this.message = message;
		this.city = city;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public APICustomResponse() {
		super();
	}

	
	public APICustomResponse(boolean status, String message) {
		super();
		this.status = status;
		this.message = message;
	}


	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
