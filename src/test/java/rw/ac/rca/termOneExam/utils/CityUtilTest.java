package rw.ac.rca.termOneExam.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;


import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.repository.ICityRepository;
import rw.ac.rca.termOneExam.service.CityService;

import java.util.ArrayList;
import java.util.List;


import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CityUtilTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @Mock
    private ICityRepository cityRepository;

    @InjectMocks
    private CityService cityService;

    @Mock
    private List<City> mockList;
    @Spy
    private List<City> spyList = new ArrayList<>();


    @Test
    public void testSpying(){
        City city = new City(101,"Nyagatare",20);
        spyList.add(city);
        Mockito.verify(spyList).add(city);
        assertEquals(1,spyList.size());
        assertEquals("Nyagatare",spyList.get(0).getName());

    }

    @Test
    public void testMocking(){
        City city = new City(101,"Kigali",20);
        mockList.add(city);
        Mockito.verify(mockList).add(city);
        assertEquals(0, mockList.size());
        assertNull(mockList.get(0));
    }



    @Test
    public void containsKigali() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/all", String.class);
        assertTrue(response.getBody().contains("{\"id\":101,\"name\":\"Kigali\",\"weather\":24.0,\"fahrenheit\":0.0}"));
    }
    @Test
    public void containsMusanze() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/all", String.class);
        assertTrue(response.getBody().contains("{\"id\":102,\"name\":\"Musanze\",\"weather\":18.0,\"fahrenheit\":0.0}"));
    }
    @Test
    public void cityLessThanTen(){
      ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/less-than/10", String.class);
        assertEquals(404,response.getStatusCodeValue());

    }

    @Test
    public void cityGreatherThanForty() {
        ResponseEntity<String> response = restTemplate.getForEntity("/api/cities/greater-than/40", String.class);
        assertEquals(404, response.getStatusCodeValue());
    }

}
