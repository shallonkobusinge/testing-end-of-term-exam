package rw.ac.rca.termOneExam.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.repository.ICityRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @Mock
    private ICityRepository cityRepository;

    @InjectMocks
    private CityService cityService;


    @Test
    public void getAll(){
        List<City> cities = Arrays.asList(new City(101, "Kigali",24), new City(102,"Musanze",18),
                new City(103,"Rubavu",20), new City(104,"Nyagatare",28));
        when(cityRepository.findAll()).thenReturn(cities);
        assertEquals(cityService.getAll().get(0), cities.get(0));
        assertTrue(cityService.getAll().size() == cities.size());


    }

    @Test
    public void  getOneById__success(){

        City city = new City(102,"Musanze",18);
        when(cityRepository.findById(102L)).thenReturn(Optional.of(city));
        assertEquals(cityService.getById(102L).getBody().getCity().getName(), "Musanze");
    }

    @Test
    public void getOneById__failure(){
        Optional emptyOptional = Optional.empty();
        when(cityRepository.findById(10L)).thenReturn(emptyOptional);

        assertEquals(cityService.getById(10L).getBody().isStatus(),false);
        assertEquals(cityService.getById(10L).getStatusCode() == HttpStatus.NOT_FOUND,true);
    }

    @Test
    public void save__success(){
        CreateCityDTO createCityDTO = new CreateCityDTO("Kamonyi",10);
        City city = new City(109,createCityDTO);
        when(cityRepository.save(any(City.class))).thenReturn(city);
        assertEquals(createCityDTO.getName(), cityService.save(createCityDTO).getBody().getCity().getName());

    }


    @Test
    public void save__failure(){
        CreateCityDTO createCityDTO = new CreateCityDTO("Kigali",24);
        City city = new City(101,createCityDTO);
        when(cityRepository.save(any(City.class))).thenReturn(city);
        when(cityRepository.existsByName(city.getName())).thenReturn(true);
        assertTrue(cityService.save(createCityDTO).getBody().isStatus() == false);


    }
    @Test
    public void existsByName__success(){
        CreateCityDTO createCityDTO = new CreateCityDTO("Kirehe", 30);
        List<City> cities = Arrays.asList(new City(101, "Kigali",24), new City(102,"Musanze",18),
                new City(103,"Rubavu",20), new City(104,"Nyagatare",28));
        City city = new City(106, createCityDTO);
        when(cityRepository.save(any(City.class))).thenReturn(city);
        when(cityRepository.findAll()).thenReturn(cities);
        assertFalse(cityService.existsByName("Kigali") == true);
    }





}
